## Як зробити щоб репозиторій оновлювався з серверу кожні 5 хвилин і після оновлення запускав наший sh файл

встановити ntp, ntpdate
```
apt-get update && apt-get install ntp ntpdate
```
копіювати репозиторій bitbucket
```
git -C /home/vagrant/ clone https://bitbucket.org/msergiy87/synchronize-time-task
```
додати в cron файл користувача root кманду
```
crontab -e
*/5 * * * *	git -C /home/vagrant/synchronize-time-task pull && bash /home/vagrant/synchronize-time-task/synchronize-time.sh
```