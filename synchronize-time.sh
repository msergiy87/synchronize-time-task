#!/bin/bash
export PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:$PATH

NTP1="ntp.time.in.ua"
NTP2="ntp2.time.in.ua"
NTP3="ntp3.time.in.ua"

service ntp stop

for NTP_SERVER in $NTP1 $NTP2 $NTP3
do
	ntpdate $NTP_SERVER
done

service ntp start